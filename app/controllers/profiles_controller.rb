class ProfilesController < ApplicationController

#將重複的函式使用 before_filter法 來省略掉

	before_action :find_profile, :only =>[:show, :edit, :update, :destroy]

	def index
		@profiles = Profile.all  
		 #@profiles = profile.page(params[:page]).per(5)

		 #因為使用了分頁外掛 kaminari，所以註解掉了

		  #respond_to do |format|
		   # format.html # index.html.erb
		    #format.xml { render :xml => @profiles.to_xml }
		    #format.json { render :json => @profiles.to_json }
		    #format.atom { @feed_title = "正心中學特約商店清單" } # index.atom.builder
		  #end
	end

	def new
		@profile = Profile.new
	end

	def create
		@profile = Profile.new(profile_params)
		
		if @profile.save
			redirect_to profiles_path
		else
			render :action => :new
		end

		flash[:notice] = "特約清單已經成功被建立"
	end

	def show
	end

	def edit
	end

	def update
		if @profile.update_attributes(profile_params)
			redirect_to profile_path(@profile)
		else
			render :action => :edit
		end

		flash[:notice] = "特約清單已經成功被更新"
	end

	def destroy
		@profile.destroy

		redirect_to profiles_path

		flash[:alert] = "特約清單已經成功被刪除"
	end

	protected  
	
	# rails 4.0 之後才有的安全設定，要宣告允許使用的變數
	# 所以將 params[:profile]以『profile_params』替代
	def profile_params
		params.require(:profile).permit(:title,:category_id,:phone,:fax,:address,:boss,:description,:pledge,:map,:photo,:promotions,:datetime,:link,:reference,:recommendation,:note)
	end 

	#配合上面before_action 來省略重複的事情
	def find_profile
		@profile = Profile.find(params[:id])
	end


end
