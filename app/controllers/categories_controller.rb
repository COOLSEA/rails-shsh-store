class CategoriesController < ApplicationController

	#將重複的函式使用 before_filter法 來省略掉

	before_action :find_category, :only =>[:show, :edit, :update, :destroy]

	def index
		#@categories = Category.all  因為使用了分頁外掛 kaminari，所以註解掉了
		@categories = Category.page(params[:page]).per(5)

		  respond_to do |format|
		    format.html # index.html.erb
		    format.xml { render :xml => @categories.to_xml }
		    format.json { render :json => @categories.to_json }
		    format.atom { @feed_title = "正心特學特約商店清單" } # index.atom.builder
		  end
	end

	def new
		@category = Category.new
	end

	def create
		@category = Category.new(category_params)
		
		if @category.save
			redirect_to categories_path
		else
			render :action => :new
		end

		flash[:notice] = "行業別已經成功被建立"
	end

	def show
		@page_title = @category.name
	end

	def edit
	end

	def update
		if @category.update_attributes(category_params)
			redirect_to category_path(@category)
		else
			render :action => :edit
		end

		flash[:notice] = "行業別已經成功被更新"
	end

	def destroy
		@category.destroy

		redirect_to categories_path

		flash[:alert] = "行業別已經成功被刪除"
	end

	protected  
	
	# rails 4.0 之後才有的安全設定，要宣告允許使用的變數
	# 所以將 params[:category]以『category_params』替代
	def category_params
		params.require(:category).permit(:name,:description)
	end 

	#配合上面before_filter 來省略重複的事情
	def find_category
		@category = Category.find(params[:id])
	end

end
