atom_feed do |feed|
  feed.title( @feed_title )
  feed.updated( @categories.last.created_at )
  @categories.each do |category|
    feed.entry(category) do |entry|
      entry.title( category.name )
      entry.content( category.description, :type => 'html' )
    end
  end
end
