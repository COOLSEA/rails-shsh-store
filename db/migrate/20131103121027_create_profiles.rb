class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.string :title
      t.integer :category_id
      t.integer :phone
      t.integer :fax
      t.string :address
      t.string :boss
      t.text :description
      t.text :pledge
      t.string :map
      t.string :photo
      t.text :promotions
      t.datetime :datetime
      t.string :link
      t.string :reference
      t.text :recommendation
      t.string :note

      t.timestamps
    end
  end
end
